package com.devcamp.task6040jpacarrelationship.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task6040jpacarrelationship.model.Car;

public interface ICarRepository extends JpaRepository<Car, Long>{
    Car findByCarCode(String carCode);
}
