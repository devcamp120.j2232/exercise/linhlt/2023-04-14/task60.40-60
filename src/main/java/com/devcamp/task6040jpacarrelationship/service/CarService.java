package com.devcamp.task6040jpacarrelationship.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task6040jpacarrelationship.model.Car;
import com.devcamp.task6040jpacarrelationship.model.CarType;
import com.devcamp.task6040jpacarrelationship.repository.ICarRepository;

@Service
public class CarService {
    @Autowired
    ICarRepository carRepository;
    public ArrayList<Car> getAllCars(){
        ArrayList<Car> carList = new ArrayList<>();
        carRepository.findAll().forEach(carList:: add);
        return carList;
    }
    public Set<CarType> getCarTypebyCarCode(String carCode){
        Car vCar = carRepository.findByCarCode(carCode);
        if (vCar != null){
            return vCar.getTypes();
        } 
        else return null;
    }
}
